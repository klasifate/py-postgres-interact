.PHONY: tests
tests:
	poetry run -- coverage run -m unittest -f --catch tests/test* && make coverage-report

coverage-report:
	make dump-coverage-report && cat ./coverage-report.md

dump-coverage-report:
	poetry run -- coverage report --format markdown --ignore-errors --omit=/tmp/*,tests/* > ./coverage-report.md