from typing import ClassVar

from settings_mng import BaseSettings, SettingsConfigDict


class OnlineOperationSettings(BaseSettings):
    host: str = "127.0.0.1"
    port: int = 5432
    username: str = "postgres"
    password: str = "postgres"
    name: str

    @property
    def url(self) -> str:
        return f"postgresql+psycopg://{self.username}:{self.password}@{self.host}:{self.port}/{self.name}"

    @property
    def url_without_driver(self) -> str:
        return f"postgresql://{self.username}:{self.password}@{self.host}:{self.port}/{self.name}"

    model_config: ClassVar[SettingsConfigDict] = SettingsConfigDict(BaseSettings.model_config, env_prefix="db_") # type: ignore[misc]


class DatabaseSettings(OnlineOperationSettings):
    pool_size: int = 20
    max_overflow: int = 0
    connect_retry: int = 20
