from typing import Type

from sqlalchemy.ext.asyncio import AsyncEngine
from sqlalchemy.ext.asyncio import create_async_engine as create_async_engine_origin
from sqlalchemy.pool import NullPool
from sqlalchemy.pool.base import Pool as BasePool


def create_async_engine(
    url: str,
    *,
    poolcls: Type[BasePool] | None = None,
    pool_size: int = 20,
    max_overflow: int = 0,
    pool_pre_ping: bool = True,
    **kwargs,
) -> AsyncEngine:
    if poolcls is not None:
        kwargs["poolclass"] = poolcls

    kwargs.update(
        {
            "pool_pre_ping": pool_pre_ping,
        },
    )

    if poolcls is not NullPool:
        kwargs.update(
            {
                "pool_size": pool_size,
                "max_overflow": max_overflow,
            },
        )

    return create_async_engine_origin(url, **kwargs)


DEFAULT_CONVENTIONS = {
    "all_column_names": lambda constraint, table: "_".join(
        [str(column.name) for column in constraint.columns.values()],
    ),
    "ix": "ix__%(table_name)s__%(all_column_names)s",
    "uq": "uq__%(table_name)s__%(all_column_names)s",
    "ck": "ck__%(table_name)s__%(constraint_name)s",
    "fk": ("fk__%(table_name)s__%(all_column_names)s__%(referred_table_name)s"),
    "pk": "pk__%(table_name)s",
}
