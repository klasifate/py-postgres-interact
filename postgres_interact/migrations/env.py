import asyncio
from typing import Any, Callable

from alembic import context
from alembic.config import Config
from sqlalchemy import pool  # type: ignore
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.ddl import _CreateBase, _DropBase

from postgres_interact.migrations.patches import DEFAULT_CLAUSE_PATCHES
from postgres_interact.utils import create_async_engine


def create_patch(visitor: Any, method: str) -> Callable:
    @compiles(visitor)
    def _add_check_existing(element: Any, compiler: Any, **kw):
        if isinstance(element, _CreateBase):
            element.if_not_exists = True
        elif isinstance(element, _DropBase):
            element.if_exists = True
        return getattr(compiler, method)(element, **kw)

    return _add_check_existing


def enable_patches(config: Config):
    always = config.attributes.get("patch_clause_always", True)
    if not always:
        return
    user_patches = config.attributes.get("clause_patches")
    for patch in DEFAULT_CLAUSE_PATCHES if user_patches else DEFAULT_CLAUSE_PATCHES:
        create_patch(*patch)


def run_migrations_offline() -> None:
    config = context.config

    url = config.get_main_option("sqlalchemy.url")
    if url is None:
        raise ValueError("Url is not set")

    base_model_class = config.attributes.get("base_model_class")
    if base_model_class is None:
        raise ValueError("No base_model_class")

    enable_patches(config)

    context.configure(
        url=url,
        target_metadata=base_model_class.metadata,
        literal_binds=True,
        dialect_name="postgresql",
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


async def run_migrations_online() -> None:
    config = context.config

    url = config.get_main_option("sqlalchemy.url")
    if url is None:
        raise ValueError("Url is not set")

    base_model_class = config.attributes.get("base_model_class")
    if base_model_class is None:
        raise ValueError("No base_model_class")

    enable_patches(config)

    engine = create_async_engine(url, poolcls=pool.NullPool)  # type: ignore

    def do_migrations(connection):
        context.configure(
            connection=connection,
            target_metadata=base_model_class.metadata,
        )

        with context.begin_transaction():
            context.run_migrations()

    async with engine.connect() as connection:
        await connection.run_sync(do_migrations)


if context.is_offline_mode():
    run_migrations_offline()
elif not context.config.attributes.get("not_run"):
    asyncio.run(run_migrations_online())
