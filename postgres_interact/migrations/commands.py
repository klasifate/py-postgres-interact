import inspect
import pathlib
import sys
from types import SimpleNamespace
from typing import Any, Callable, TypeAlias, cast

import click
from alembic import __version__, command, util
from alembic.config import Config
from alembic.util import compat

from ..settings import OnlineOperationSettings

RevisionDir: TypeAlias = str | pathlib.Path
ModelCls: TypeAlias = Any
RetrieveOnlineParams: TypeAlias = Callable[[], tuple[OnlineOperationSettings, RevisionDir, Any]]
RetrieveOfflineParams: TypeAlias = Callable[[], tuple[RevisionDir, Any]]
ModifyConfigFunction: TypeAlias = Callable[[Config], Config]

# Список операций, для которых требуется установка URL из настроек
ONLINE_OPERATIONS = [
    command.check,
    command.current,
    command.downgrade,
    command.ensure_version,
    command.revision,
    command.stamp,
    command.upgrade,
]

# Отключенные операции
EXCLUDE = [
    command.edit,
    command.init,
    command.list_templates,
]

positional_translations = {command.stamp: {"revision": "revisions"}}

kwargs_opts = {
    "message": (
        ("-m", "--message"),
        {"type": str, "help": "Message string to use with 'revision'"},
    ),
    "sql": (
        ("--sql",),
        {
            "is_flag": True,
            "help": "Don't emit SQL to database - dump to standard output/file instead. See docs on offline mode.",
        },
    ),
    "tag": (
        ("--tag",),
        {"type": str, "help": "Arbitrary 'tag' name - can be used by custom env.py scripts."},
    ),
    "head": (
        ("--head",),
        {"type": str, "help": "Specify head revision or <branchname>@head to base new revision on."},
    ),
    "splice": (
        ("--splice",),
        {"is_flag": True, "help": "Allow a non-head revision as the 'head' to splice onto"},
    ),
    "depends_on": (
        ("--depends-on",),
        {"multiple": True, "help": "Specify one or more revision identifiers which this revision should depend on."},
    ),
    "rev_id": (
        ("--rev-id",),
        {"type": str, "help": "Specify a hardcoded revision id instead of generating one"},
    ),
    "branch_label": (
        ("--branch-label",),
        {"type": str, "help": "Specify a branch label to apply to the new revision"},
    ),
    "verbose": (
        ("-v", "--verbose"),
        {"is_flag": True, "help": "Use more verbose output"},
    ),
    "resolve_dependencies": (
        ("--resolve-dependencies",),
        {"is_flag": True, "help": "Treat dependency versions as down revisions"},
    ),
    "autogenerate": (
        ("--autogenerate",),
        {
            "is_flag": True,
            "help": "Populate revision script with candidate migration operations, based on comparison of database to model.",  # noqa: E501
        },
    ),
    "rev_range": (
        ("-r", "--rev-range"),
        {"type": str, "help": "Specify a revision range; format is [start]:[end]"},
    ),
    "indicate_current": (
        ("-i", "--indicate-current"),
        {"is_flag": True, "help": "Indicate the current revision"},
    ),
    "purge": (
        ("--purge",),
        {"is_flag": True, "help": "Unconditionally erase the version table before stamping"},
    ),
}

positional_help = {
    "directory": "location of scripts directory",
    "revision": "revision identifier",
    "revisions": "one or more revisions, or 'heads' for all heads",
}


def cli_wrapper(
    fn: Callable,
    args_list: list[str],
    kwargs_list: list[str],
    retrieve: RetrieveOfflineParams | RetrieveOnlineParams,
    modify_config: ModifyConfigFunction | None,
):
    @click.pass_context
    def wrapper(ctx: click.Context, **click_options: Any):
        # Объединяем глобальные опции, переданные через контекст
        if ctx.obj:
            click_options = {**ctx.obj, **click_options}
        options = SimpleNamespace(**click_options)
        config = Config(
            cmd_opts=options,  # type: ignore[arg-type]
            stdout=sys.stdout,
        )
        env_dir_location = pathlib.Path(__file__).parent
        config.set_main_option("script_location", str(env_dir_location))

        if fn in ONLINE_OPERATIONS:
            settings, revision_dir, base_model_cls = cast(RetrieveOnlineParams, retrieve)()
            config.set_main_option("sqlalchemy.url", settings.url)
        else:
            revision_dir, base_model_cls = cast(RetrieveOfflineParams, retrieve)()
        config.set_main_option("version_locations", str(revision_dir))
        config.attributes["base_model_class"] = base_model_cls

        if modify_config:
            config = modify_config(config)

        try:
            fn(
                config,
                *[getattr(options, k, None) for k in args_list],
                **{k: getattr(options, k, None) for k in kwargs_list},
            )
        except util.CommandError as error:
            if getattr(options, "raiseerr", False):
                raise
            util.err(str(error))

    return wrapper


def create_click_command(
    fn: Callable,
    retrieve_online: RetrieveOnlineParams,
    retrieve_offline: RetrieveOfflineParams,
    modify_config: ModifyConfigFunction | None,
):
    spec = compat.inspect_getfullargspec(fn)
    args = spec.args[1:]
    if spec.defaults is not None:
        kwargs_list = args[-len(spec.defaults) :]
        pos_args = args[: -len(spec.defaults)]
    else:
        kwargs_list = []
        pos_args = args

    # Применяем перевод для позиционных аргументов, если необходимо
    if fn in positional_translations:
        pos_args = [positional_translations[fn].get(arg, arg) for arg in pos_args]

    params: list[click.Parameter] = []
    # Добавляем позиционные аргументы
    for arg in pos_args:
        if arg == "revisions":
            params.append(click.Argument([arg], nargs=-1, required=True, metavar=arg.upper()))
        else:
            params.append(click.Argument([arg], required=True, metavar=arg.upper()))

    # Добавляем опции для именованных аргументов
    for kw in kwargs_list:
        if kw in kwargs_opts:
            flags, option_kwargs = kwargs_opts[kw]
            params.append(click.Option(list(flags), **option_kwargs))  # type: ignore[arg-type]
        else:
            params.append(click.Option([f"--{kw}"], type=str))

    # Извлекаем справочный текст из док-строки функции
    help_text = ""
    if fn.__doc__:
        lines = fn.__doc__.splitlines()
        help_lines = []
        for line in lines:
            if line.strip() == "":
                break
            help_lines.append(line.strip())
        help_text = " ".join(help_lines)

    retrieve = retrieve_online if fn in ONLINE_OPERATIONS else retrieve_offline

    callback = cli_wrapper(fn, pos_args, kwargs_list, retrieve, modify_config)
    return click.Command(name=fn.__name__, callback=callback, params=params, help=help_text)


def create_cli(
    retrieve_online: RetrieveOnlineParams,
    retrieve_offline: RetrieveOfflineParams,
    modify_config: ModifyConfigFunction | None = None,
) -> click.Group:
    @click.group()
    @click.version_option(version=__version__)
    @click.option("-x", multiple=True, help="Additional arguments for custom env.py scripts")
    @click.option("--raiseerr", is_flag=True, help="Raise a full stack trace on error")
    @click.pass_context
    def migrations(ctx: click.Context, x: list[Any], raiseerr: bool):  # noqa: FBT001
        ctx.obj = {"x": list(x), "raiseerr": raiseerr}

    # Перебираем функции из alembic.command и добавляем их как команды CLI
    for attr in dir(command):
        fn = getattr(command, attr)
        if not (inspect.isfunction(fn) and fn.__name__[0] != "_" and fn.__module__ == "alembic.command"):
            continue
        if fn in EXCLUDE:
            continue

        cmd = create_click_command(fn, retrieve_online, retrieve_offline, modify_config)
        migrations.add_command(cmd)
    return migrations
