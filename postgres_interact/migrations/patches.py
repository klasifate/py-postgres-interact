from typing import Iterable

from sqlalchemy.dialects.postgresql import DropEnumType
from sqlalchemy.schema import (
    CreateIndex,
    CreateSequence,
    CreateTable,
    DropIndex,
    DropSequence,
    DropTable,
)
from sqlalchemy.sql.ddl import _CreateBase, _DropBase

Patches = Iterable[tuple[type[_CreateBase] | type[_DropBase], str]]


DEFAULT_CLAUSE_PATCHES: Patches = (  # type: ignore[assignment]
    (CreateTable, "visit_create_table"),
    (CreateIndex, "visit_create_index"),
    (CreateSequence, "visit_create_sequence"),
    (DropTable, "visit_drop_table"),
    (DropSequence, "visit_drop_sequence"),
    (DropIndex, "visit_drop_index"),
    (DropEnumType, "visit_drop_enum_type"),
)
