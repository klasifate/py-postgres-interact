import abc
import asyncio
import io
from pathlib import Path
from typing import cast
from unittest import IsolatedAsyncioTestCase
from uuid import uuid4

from alembic import command as alembic_commands
from alembic.config import Config

from ..migrations import ModelCls, RevisionDir
from ..session_manager import SessionManager
from ..settings import DatabaseSettings, OnlineOperationSettings
from .utils import (
    InstallationInfo,
    create_alembic_config,
    create_test_db,
    downgrade_db,
    drop_postgres,
    drop_test_db,
    set_up_postgres,
    upgrade_db,
)


def format_not_implemented_error(obj: IsolatedAsyncioTestCase, necessary_parent: type[IsolatedAsyncioTestCase]) -> str:
    return f"No implementation provided and default one doesn't work because \
{obj.__class__.__module__}.{obj.__class__.__name__} \
doesn't inherit {necessary_parent.__module__}.{necessary_parent.__name__}"


class SetUpPostgresFixture(IsolatedAsyncioTestCase):
    _installation_info: InstallationInfo | None = None
    _config_path: str | Path | None = None
    _set_up_count = 0
    # lock is used for parallelism
    _lock = asyncio.Lock()

    @classmethod
    async def _set_up_postgres(cls) -> None:
        async with cls._lock:
            if cls._set_up_count == 0:
                cls._installation_info, cls._config_path = await set_up_postgres()
            cls._set_up_count += 1

    @classmethod
    async def _drop_postgres(cls) -> None:
        async with cls._lock:
            cls._set_up_count -= 1
            if cls._set_up_count == 0:
                await drop_postgres(cast(str | Path, cls._config_path))
                cls._installation_info = None

    @property
    def require_to_set_up(self) -> bool:
        return True

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        if not self.require_to_set_up:
            return
        try:
            await self._set_up_postgres()
        except Exception:
            await super().asyncTearDown()
            raise

    async def asyncTearDown(self) -> None:
        try:
            if not self.require_to_set_up:
                return
            await self._drop_postgres()
        finally:
            await super().asyncTearDown()

    def get_installation_info(self) -> InstallationInfo:
        if not self.require_to_set_up:
            raise NotImplementedError('No custom implementation of retrieving installation info')
        if self._set_up_count == 0:
            raise ValueError("This method can be called only in opened state")
        return cast(InstallationInfo, self._installation_info)


class CreateDbFixture(IsolatedAsyncioTestCase):
    @classmethod
    def gen_temp_name(cls) -> str:
        return "test" + str(uuid4()).replace("-", "")

    @property
    def db_settings(self) -> OnlineOperationSettings:
        if hasattr(self, "_db_settings"):
            return self._db_settings  # type: ignore[has-type]

        if SetUpPostgresFixture in (mro := self.__class__.mro()) and mro.index(CreateDbFixture) < mro.index(
            SetUpPostgresFixture,
        ):
            installation_info = cast(SetUpPostgresFixture, self).get_installation_info()

            self._db_settings = OnlineOperationSettings(
                host=installation_info["host"],
                port=installation_info["port"],
                username=installation_info["username"],
                password=installation_info["password"],
                name=self.gen_temp_name(),
            )
            return self._db_settings

        raise NotImplementedError(format_not_implemented_error(self, SetUpPostgresFixture))

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        try:
            await create_test_db(self.db_settings)
        except Exception:
            await super().asyncTearDown()
            raise

    async def asyncTearDown(self) -> None:
        try:
            await drop_test_db(self.db_settings)
        finally:
            await super().asyncTearDown()


class ForMigrations(abc.ABC, IsolatedAsyncioTestCase):
    @property
    @abc.abstractmethod
    def revisions_dir(self) -> RevisionDir:
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def base_model_cls(self) -> ModelCls:
        raise NotImplementedError

    @property
    def db_settings(self) -> OnlineOperationSettings:
        if CreateDbFixture in (mro := self.__class__.mro()) and mro.index(ForMigrations) < mro.index(
            CreateDbFixture,
        ):
            return super().db_settings  # type: ignore[misc]
        raise NotImplementedError(format_not_implemented_error(self, CreateDbFixture))


class MigrateDBFixture(ForMigrations):
    alembic_config: Config

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()

        self.alembic_config = create_alembic_config(
            self.db_settings.url,
            self.revisions_dir,
            self.base_model_cls,
        )

        try:
            await upgrade_db(self.alembic_config)
        except Exception:
            await super().asyncTearDown()
            raise

    async def asyncTearDown(self) -> None:
        try:
            await downgrade_db(self.alembic_config)
        finally:
            await super().asyncTearDown()


class CreateSessionManagerFixture(IsolatedAsyncioTestCase):
    session_manager: SessionManager

    @property
    def open_initial_session(self) -> bool:
        return False

    @property
    def db_settings(self) -> DatabaseSettings:
        if CreateDbFixture in (mro := self.__class__.mro()) and mro.index(CreateSessionManagerFixture) < mro.index(
            CreateDbFixture,
        ):
            return DatabaseSettings(**super().db_settings.model_dump())  # type: ignore[misc]
        raise NotImplementedError(format_not_implemented_error(self, CreateDbFixture))

    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()
        self.session_manager = SessionManager(self.db_settings)

        if not self.open_initial_session:
            return

        self._session_context = await self.session_manager.per_request().__aenter__()

    async def asyncTearDown(self) -> None:
        try:
            if self.open_initial_session:
                await self._session_context.__aexit__(None, None, None)
            await self.session_manager.close()
        finally:
            await super().asyncTearDown()


class StairTest(ForMigrations):
    async def run_test(self) -> None:
        stdout = io.StringIO()
        config = create_alembic_config(
            self.db_settings.url,
            self.revisions_dir,
            self.base_model_cls,
            stdout=stdout,
        )
        loop = asyncio.get_running_loop()
        await loop.run_in_executor(None, alembic_commands.history, config)

        revisions_count = len(stdout.getvalue().strip().split("\n"))
        for to_revision in range(revisions_count):
            to_revision += 1
            for __ in range(to_revision):
                await upgrade_db(config, "+1")
            for __ in range(to_revision):
                await downgrade_db(config, "-1")
