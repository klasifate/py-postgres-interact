import asyncio
import os
import subprocess
import sys
from pathlib import Path
from tempfile import mkstemp
from typing import TextIO, TypedDict

import psycopg
from alembic.config import Config
from alembic.runtime.environment import EnvironmentContext
from alembic.script import ScriptDirectory

from ..migrations import ModelCls, RevisionDir
from ..settings import OnlineOperationSettings


async def create_test_db(
    settings: OnlineOperationSettings,
    *,
    main_db_name: str = "postgres",
) -> None:
    auxiliary_db_settings = settings.model_copy(update={"name": main_db_name})

    async with await psycopg.AsyncConnection.connect(
        auxiliary_db_settings.url_without_driver,
        autocommit=True,
    ) as conn:
        await conn.execute(
            f'CREATE DATABASE "{settings.name}" OWNER "{settings.username}";',
        )


async def drop_test_db(
    settings: OnlineOperationSettings,
    *,
    main_db_name: str = "postgres",
) -> None:
    auxiliary_db_settings = settings.model_copy(update={"name": main_db_name})
    async with await psycopg.AsyncConnection.connect(
        auxiliary_db_settings.url_without_driver,
        autocommit=True,
    ) as conn:
        await conn.execute(f'DROP DATABASE "{settings.name}"')


DOCKER_COMPOSE_CONFIG = """
version: "3.3"
services:
  db:
    image: postgres:latest
    restart: always
    ports:
      - {port}:5432
    environment:
      - POSTGRES_USER={username}
      - POSTGRES_PASSWORD={password}
"""


class InstallationInfo(TypedDict):
    username: str
    password: str
    port: int
    host: str


async def set_up_postgres(
    *,
    username: str | None = None,
    password: str | None = None,
    port: int | None = None,
    timeout: int | float = 10,
) -> tuple[InstallationInfo, str | Path]:
    if not username:
        username = "postgres"
    if not password:
        password = "12345678"  # noqa: S105
    if not port:
        port = 5432
    host = "127.0.0.1"

    config = DOCKER_COMPOSE_CONFIG.format(username=username, password=password, port=port)

    def create() -> str:
        fd, filename = mkstemp(prefix="test_set_up_db")
        with open(fd, "wt") as file:
            file.write(config)
        return filename

    loop = asyncio.get_running_loop()
    config_path = await loop.run_in_executor(None, create)

    cmd = f"docker compose -f {config_path} up -d"
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdin=subprocess.DEVNULL,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    exitcode = await process.wait()
    if exitcode:
        # try\except block is used to catch potential errors in drop_postgres and saving the original error
        try:
            stdout = (await process.stdout.read()).decode("utf-8")  # type: ignore[union-attr]
            raise ValueError("Some error happened while set up db using docker compose. Output:\n" + stdout) # noqa: TRY301
        except ValueError:
            await drop_postgres(config_path)
            raise

    url = OnlineOperationSettings(
        host=host,
        port=port,
        username=username,
        password=password,
        name="postgres",
    ).url_without_driver

    passed = 0
    while passed <= timeout:
        try:
            async with await psycopg.AsyncConnection.connect(url):
                break
        except psycopg.OperationalError:
            await asyncio.sleep(1)
            passed += 1
    else:
        raise TimeoutError("Not started")

    return {
        "password": password,
        "username": username,
        "port": port,
        "host": host,
    }, config_path


async def drop_postgres(config_path: str | Path) -> None:
    cmd = f"docker compose -f {config_path} down"
    process = await asyncio.create_subprocess_shell(
        cmd,
        stdin=subprocess.DEVNULL,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    exitcode = await process.wait()
    if exitcode:
        stdout = (await process.stdout.read()).decode("utf-8")  # type: ignore[union-attr]
        raise ValueError("Some error happened while drop db using docker compose. Output:\n" + stdout)

    def remove() -> None:
        os.remove(config_path)

    loop = asyncio.get_running_loop()
    loop.run_in_executor(None, remove)


def create_alembic_config(
    url: str,
    path_to_revisions: RevisionDir,
    base_model_cls: ModelCls,
    *,
    stdout: TextIO | None = None,
) -> Config:
    lib_dir = Path(__file__).parent.parent
    env_dir = lib_dir / "migrations"
    if stdout is None:
        stdout = sys.stdout

    config = Config(cmd_opts=None, stdout=stdout)
    config.set_main_option("script_location", str(env_dir))
    config.set_main_option("sqlalchemy.url", url)
    config.set_main_option("version_locations", str(path_to_revisions))
    config.attributes["base_model_class"] = base_model_cls
    config.attributes["not_run"] = True

    return config


async def upgrade_db(config: Config, revision: str = "head", starting_rev: str | None = None) -> None:
    script = ScriptDirectory.from_config(config)

    def upgrade(rev, __):
        return script._upgrade_revs(revision, rev)  # noqa: SLF001

    with EnvironmentContext(
        config,
        script,
        fn=upgrade,
        as_sql=False,
        starting_rev=starting_rev,
        destination_rev=revision,
        tag=None,
    ):
        from ..migrations.env import run_migrations_online

        await run_migrations_online()


async def downgrade_db(config: Config, revision: str = "base", starting_rev: str | None = None) -> None:
    script = ScriptDirectory.from_config(config)

    def downgrade(rev, context):
        return script._downgrade_revs(revision, rev)  # noqa: SLF001

    with EnvironmentContext(
        config,
        script,
        fn=downgrade,
        as_sql=False,
        starting_rev=starting_rev,
        destination_rev=revision,
    ):
        from ..migrations.env import run_migrations_online

        await run_migrations_online()
