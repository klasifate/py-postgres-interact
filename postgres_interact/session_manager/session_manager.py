from contextlib import asynccontextmanager
from typing import AsyncGenerator

from dependency_injector.resources import AsyncResource
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession

from ..settings import DatabaseSettings
from ..utils import create_async_engine
from .context import (
    SessionContext,
)
from .levels import DEFAULT_ISOLATION_LEVEL, IsolationLevel
from .utils import async_session_maker, copy_async_engine_and_session_cls, create_async_session_cls


class _EnginePool:
    def __init__(
        self,
        settings: DatabaseSettings,
        *,
        start_isolation_level: IsolationLevel = DEFAULT_ISOLATION_LEVEL,
    ) -> None:
        self._isolation_levels: dict[IsolationLevel, tuple[AsyncEngine, async_session_maker]] = {}
        engine = create_async_engine(
            settings.url,
            pool_size=settings.pool_size,
            max_overflow=settings.max_overflow,
            isolation_level=start_isolation_level,
        )
        session_maker = create_async_session_cls(engine)
        self._isolation_levels[start_isolation_level] = (engine, session_maker)  # type: ignore[index]

    def get_any_engine(self) -> AsyncEngine:
        return list(self._isolation_levels.values())[0][0]

    def get_engine(
        self,
        isolation_level: IsolationLevel,
    ) -> tuple[AsyncEngine, async_session_maker]:
        engine = self._isolation_levels.get(isolation_level)  # type: ignore[arg-type]
        if engine is not None:
            return engine

        pair = copy_async_engine_and_session_cls(
            self.get_any_engine(),
            isolation_level,
        )
        self._isolation_levels[isolation_level] = pair  # type: ignore[index]
        return pair


class SessionManager:
    def __init__(self, settings: DatabaseSettings) -> None:
        self._init(settings)

    def _init(self, settings: DatabaseSettings) -> None:
        self._engine_pool = _EnginePool(settings, start_isolation_level=IsolationLevel.READ_COMMITTED)
        self._settings = settings

    def session_maker(
        self,
        isolation_level: IsolationLevel = DEFAULT_ISOLATION_LEVEL,
    ) -> async_session_maker:
        return self._engine_pool.get_engine(isolation_level)[1]

    def per_request(
        self,
        default_isolation_level: IsolationLevel = DEFAULT_ISOLATION_LEVEL,
    ) -> SessionContext:
        return SessionContext(self, default_isolation_level=default_isolation_level)  # type: ignore[return-value]

    @asynccontextmanager
    async def begin_session(
        self,
        *,
        isolation_level: IsolationLevel = DEFAULT_ISOLATION_LEVEL,
        new_session_if_necessary: bool = False,
    ) -> AsyncGenerator[AsyncSession, None]:
        context = SessionContext.get_context()

        async with (
            context.begin_session(
                isolation_level=isolation_level,
                new_session_if_necessary=new_session_if_necessary,
            ) as session,
        ):
            yield session

    async def close(self) -> None:
        await self._engine_pool.get_any_engine().dispose()

    async def refresh(self) -> None:
        await self.close()
        self._init(self._settings)


class SessionManagerResource(AsyncResource):
    async def init(self, settings: DatabaseSettings) -> SessionManager:
        return SessionManager(settings)

    async def shutdown(self, resource: SessionManager) -> None:  # type: ignore
        await resource.close()
