from enum import Enum


def compare(level1: "IsolationLevel", level2: "IsolationLevel") -> int:
    if level1 == level2:
        return 0

    levels = [
        IsolationLevel.READ_UNCOMMITTED,
        IsolationLevel.READ_COMMITTED,
        IsolationLevel.REPEATABLE_READ,
        IsolationLevel.SERIALIZABLE,
    ]

    if levels.index(level1) < levels.index(level2):
        return -1

    return 1


class IsolationLevel(str, Enum):
    READ_UNCOMMITTED = "READ UNCOMMITTED"
    READ_COMMITTED = "READ COMMITTED"
    REPEATABLE_READ = "REPEATABLE READ"
    SERIALIZABLE = "SERIALIZABLE"

    compare = compare


DEFAULT_ISOLATION_LEVEL = IsolationLevel.READ_COMMITTED
