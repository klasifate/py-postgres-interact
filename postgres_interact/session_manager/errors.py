def format_msg(msg: str) -> str:
    return " ".join(striped for line in msg.split("\n") if (striped := line.strip()))


class BaseError(Exception):
    def __init__(self, msg: str) -> None:
        super().__init__(format_msg(msg))

    @property
    def msg(self) -> str:
        return self.args[0]

    def __str__(self) -> str:
        return self.msg

    def __repr__(self) -> str:
        return f"{self.__class__.__module__}.{self.__class__.__name__}: {self.msg}"


class SessionCantBeProvidedError(BaseError, ValueError):
    pass


class SessionContextNotFoundError(BaseError, LookupError):
    pass


class SessionContextAlreadyExistsError(BaseError, RuntimeError):
    pass
