import warnings
from contextlib import asynccontextmanager
from contextvars import ContextVar
from types import TracebackType
from typing import TYPE_CHECKING, Any, AsyncGenerator, NamedTuple, Union

from sqlalchemy.ext.asyncio import AsyncSession, AsyncSessionTransaction

from .errors import (
    SessionCantBeProvidedError,
    SessionContextAlreadyExistsError,
    SessionContextNotFoundError,
)

if TYPE_CHECKING:
    from .session_manager import SessionManager
from .levels import DEFAULT_ISOLATION_LEVEL, IsolationLevel


class AsyncNestedSession(AsyncSession):
    def __init__(self, savepoint: AsyncSessionTransaction):
        self.savepoint = savepoint

    def __getattribute__(self, name: str) -> Any:
        if name not in ["commit", "rollback", "savepoint", "origin_session"]:
            savepoint = super().__getattribute__("savepoint")
            return getattr(savepoint.session, name)
        return super().__getattribute__(name)

    async def commit(self) -> None:
        return await self.savepoint.commit()

    async def rollback(self) -> None:
        return await self.savepoint.rollback()

    @property
    def origin_session(self) -> AsyncSession:
        return self.savepoint.session


class _SessionInfo(NamedTuple):
    session: AsyncSession
    isolation_level: IsolationLevel
    is_nested: bool


class SessionContext:
    def __init__(
        self,
        session_manager: "SessionManager",
        *,
        default_isolation_level: IsolationLevel = DEFAULT_ISOLATION_LEVEL,
    ) -> None:
        self.opened = False
        self.closed = False
        self.session_manager = session_manager
        self.default_isolation_level = default_isolation_level

        self._sessions: list[_SessionInfo] = []

    _context_var = ContextVar[Union[None, "SessionContext"]]("session_context", default=None)

    @classmethod
    def get_context(cls) -> "SessionContext":
        context = cls._context_var.get()
        if context is None:
            raise SessionContextNotFoundError("Session context not found")
        return context

    async def open(self) -> "SessionContext":
        if self.opened:
            raise ValueError("Invalid state. Already opened")

        if self._context_var.get() is not None:
            raise SessionContextAlreadyExistsError("Session context already exists")
        self._context_var.set(self)

        self.opened = True
        return self

    async def close(self, *, rollback: bool = False) -> None:
        if not self.opened or self.closed:
            raise ValueError("Invalid state. Not opened or already closed")
        self.closed = True
        self._context_var.set(None)

        if not self._sessions:
            return
        session = self._sessions[0].session
        if rollback:
            await session.rollback()
        else:
            await session.commit()
        await session.__aexit__(None, None, None)

    async def __aenter__(self) -> "SessionContext":
        await self.open()
        return self

    async def __aexit__(self, exc_cls: type[Exception] | None, exc: Exception | None, tb: TracebackType | None) -> None:
        await self.close(rollback=exc is not None)

    @asynccontextmanager
    async def begin_session(
        self,
        isolation_level: IsolationLevel | None = None,
        *,
        new_session_if_necessary: bool = False,
    ) -> AsyncGenerator[AsyncSession, Any]:
        if not self.opened:
            raise ValueError("Invalid state. Not opened")

        if isolation_level is None:
            isolation_level = self.default_isolation_level

        # find last session or open an one if there are no sessions
        session: AsyncSession
        session_isolation_level: IsolationLevel
        for i in range(len(self._sessions)):
            if not (tmp := self._sessions[-i]).is_nested:
                session = tmp.session
                session_isolation_level = tmp.isolation_level
                break
        else:
            if self.default_isolation_level.compare(isolation_level) < 0:
                warnings.warn(
                    "Default isolation level is lower than the requested isolation level",
                    stacklevel=2,
                )
            else:
                isolation_level = self.default_isolation_level
            session_maker = self.session_manager.session_maker(isolation_level)
            session = await session_maker().__aenter__()
            session_isolation_level = isolation_level
            self._sessions.append(_SessionInfo(session, isolation_level, False))  # noqa: FBT003

        # open sub-transaction (savepoint)
        if session_isolation_level.compare(isolation_level) >= 0:
            async with session.begin_nested() as savepoint:
                nested_session = AsyncNestedSession(savepoint)
                self._sessions.append(_SessionInfo(nested_session, isolation_level, True))  # noqa: FBT003
                try:
                    yield nested_session
                finally:
                    self._sessions.pop(-1)
            return

        if new_session_if_necessary:
            session_maker = self.session_manager.session_maker(isolation_level)
            async with session_maker() as session:
                self._sessions.append(_SessionInfo(session, isolation_level, False))  # noqa: FBT003
                try:
                    yield session
                finally:
                    self._sessions.pop(-1)
            return

        raise SessionCantBeProvidedError(
            "New session began can't be provided because there is already a session \
and isolation level is not conformed",
        )
