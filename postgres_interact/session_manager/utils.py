from contextlib import AbstractAsyncContextManager

from sqlalchemy.ext.asyncio import (
    AsyncEngine,
    AsyncSession,
)
from sqlalchemy.orm import sessionmaker


class async_session_maker(sessionmaker[AsyncSession]):  # type: ignore[type-var] # noqa: N801
    def begin(self) -> AbstractAsyncContextManager[AsyncSession]:  # type: ignore
        return super().begin()  # type: ignore


def create_async_session_cls(engine: AsyncEngine) -> async_session_maker:
    return async_session_maker(bind=engine, expire_on_commit=False, class_=AsyncSession)  # type: ignore


def copy_async_engine_and_session_cls(
    engine: AsyncEngine,
    isolation_level: str,
) -> tuple[AsyncEngine, async_session_maker]:
    # new engine will be use the same connection pool
    new_engine = engine.execution_options(isolation_level=isolation_level)
    new_async_session_class = create_async_session_cls(new_engine)

    return new_engine, new_async_session_class
