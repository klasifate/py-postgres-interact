# Description
This package stores code for interact with db. There are such features as a session manager, migrations and utils for tests.

## Migrations
Migrations are based on alembic and their main feature is ability to integrate with client cli. It is available by providing subparser object of an user's argument parser. See `postgres_interact.migrations` for more information.

## Session manager
This object controls sessions to interact with db. It can provide them with opened transaction and track them on stack of calls to provide sub transactions or to provide new separate session unless requested transaction levels are conformed. Subtransactions are implemented by postgresql's savepoint mechanism. See `postgres_interact.session_manager` for more information.

## Utils for tests
They are included features to create postgres instances by docker, create database and apply migrations, also such feature as stair test. Also there are already implemented fixtures with this features for unittest.  See `postgres_interact.tests` for more information.

## Testing
1. Run tests
```bash 
make run-tests
```
2. Get coverage report of tests
```bash 
make coverage-report
```

## TODO
1. Add documentation
2. Fix typing of "isolate" function
