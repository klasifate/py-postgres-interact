import sqlalchemy as sa
from sqlalchemy import MetaData
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base

from postgres_interact import DEFAULT_CONVENTIONS

metadata = MetaData(naming_convention=DEFAULT_CONVENTIONS) # type: ignore[arg-type]
BaseModel = declarative_base(metadata=metadata)


class ItemModel(BaseModel): # type: ignore[misc,valid-type]
    __tablename__ = "test_models"
    __mapper_args__ = {"eager_defaults": True}

    id = sa.Column(
        UUID(as_uuid=True),
        primary_key=True,
        server_default=sa.text("uuid_generate_v1mc()"),
    )

    data = sa.Column(sa.VARCHAR(256), nullable=False)
