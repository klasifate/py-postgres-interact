import asyncio
import shutil
from tempfile import mkdtemp

from click.testing import CliRunner

from postgres_interact.migrations import create_cli
from postgres_interact.tests import CreateDbFixture, SetUpPostgresFixture

from .utils import base_model_cls, revisions_dir


class Common(CreateDbFixture, SetUpPostgresFixture):
    async def asyncSetUp(self) -> None:
        await super().asyncSetUp()

        # Создаём временную директорию с миграциями
        def create():
            dirpath = mkdtemp(prefix="test_migrations_commands_")
            shutil.copytree(revisions_dir(), dirpath, dirs_exist_ok=True)
            return dirpath

        loop = asyncio.get_running_loop()
        self.path_to_revisions = await loop.run_in_executor(None, create)

        model_cls = base_model_cls()
        self.cli = create_cli(
            retrieve_online=lambda: (self.db_settings, self.path_to_revisions, model_cls),
            retrieve_offline=lambda: (self.path_to_revisions, model_cls),
            modify_config=None,
        )
        self.runner = CliRunner()

    async def asyncTearDown(self) -> None:
        try:
            await asyncio.get_running_loop().run_in_executor(None, shutil.rmtree, self.path_to_revisions)
        finally:
            await super().asyncTearDown()

    async def run_cmd(self, *args: str) -> None:
        def run() -> None:
            result = self.runner.invoke(self.cli, list(args))
            if result.exception:
                raise result.exception

        loop = asyncio.get_running_loop()
        await loop.run_in_executor(None, run)


class TestMigrationsCommands(Common):
    async def test_upgrade(self) -> None:
        await self.run_cmd("upgrade", "head")

    async def test_downgrade(self) -> None:
        await self.run_cmd("upgrade", "head")
        await self.run_cmd("downgrade", "base")

    async def test_branches(self) -> None:
        await self.run_cmd("branches")

    async def test_check(self) -> None:
        # Ожидается, что команда 'check' завершится с ошибкой (SystemExit)
        with self.assertRaises(SystemExit):
            await self.run_cmd("check")

    async def test_current(self) -> None:
        await self.run_cmd("current")

    async def test_ensure_version(self) -> None:
        await self.run_cmd("ensure_version")

    async def test_heads(self) -> None:
        await self.run_cmd("heads")

    async def test_history(self) -> None:
        await self.run_cmd("history")

    async def test_stamp(self) -> None:
        await self.run_cmd("stamp", "heads")


class TestMigrationsCommands2(Common):
    async def test_revision(self) -> None:
        await self.run_cmd("upgrade", "head")
        await self.run_cmd("revision", "--autogenerate")

    async def test_merge(self) -> None:
        await self.run_cmd("upgrade", "head")
        await self.run_cmd("revision", "--autogenerate")
        await self.run_cmd("merge", "heads")

    async def test_show(self) -> None:
        await self.run_cmd("show", "e65dd7ef1192")
