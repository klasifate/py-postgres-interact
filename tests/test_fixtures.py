from pathlib import Path
from typing import Any

import psycopg

from postgres_interact.migrations import OnlineOperationSettings
from postgres_interact.tests import (
    CreateDbFixture,
    CreateSessionManagerFixture,
    InstallationInfo,
    MigrateDBFixture,
    SetUpPostgresFixture,
    StairTest,
)

from .models import ItemModel
from .utils import base_model_cls, revisions_dir


class TestSetUpFixture(SetUpPostgresFixture):
    async def test(self) -> None:
        # check if asyncSetUp work correctly
        info = self.get_installation_info()
        settings = OnlineOperationSettings(
            host=info["host"],
            port=info["port"],
            username=info["username"],
            password=info["password"],
            name="postgres",
        )
        async with await psycopg.AsyncConnection.connect(
            settings.url_without_driver,
        ) as conn:
            await conn.execute(
                "SELECT now();",
            )


class TestTestSetUpFixtureWithExternal(SetUpPostgresFixture):
    @property
    def require_set_up(self) -> bool:
        return False

    def get_installation_info(self) -> InstallationInfo:
        return {
            "password": "12345678",
            "username": "pupalupa",
            "port": 5432,
            "host": "127.0.0.1",
        }

    def test(self) -> None:
        pass


class TestCreateDbFixture(CreateDbFixture, SetUpPostgresFixture):
    async def test(self) -> None:
        # check if asyncSetUp work correctly
        async with await psycopg.AsyncConnection.connect(
            self.db_settings.url_without_driver,
        ) as conn:
            await conn.execute(
                "SELECT now();",
            )


class TestMigrateDBFixture(MigrateDBFixture, CreateDbFixture, SetUpPostgresFixture):
    @property
    def revisions_dir(self) -> Path:
        return revisions_dir()

    @property
    def base_model_cls(self) -> None:
        return base_model_cls()

    async def test(self) -> None:
        async with (
            await psycopg.AsyncConnection.connect(
                self.db_settings.url_without_driver,
            ) as conn,
            conn.cursor() as cursor,
        ):
            data = "some_string"
            await cursor.execute(f"INSERT INTO test_models (data) VALUES ('{data}');")  # noqa: S608
            await cursor.execute(
                "SELECT id, data FROM test_models LIMIT 1",
            )
            row = await cursor.fetchone()
            self.assertIsNotNone(row)
            self.assertEqual(row[1], data)  # type: ignore[index]


class TestCreateSessionManagerFixture(
    CreateSessionManagerFixture,
    MigrateDBFixture,
    CreateDbFixture,
    SetUpPostgresFixture,
):
    @property
    def revisions_dir(self) -> Path:
        return revisions_dir()

    @property
    def base_model_cls(self) -> Any:
        return base_model_cls()

    @property
    def open_initial_session(self) -> bool:
        return True

    async def test(self) -> None:
        data = "some_data"
        async with self.session_manager.begin_session() as session:
            session.add(ItemModel(data=data))
            await session.commit()


class StairTestOfMigrations(StairTest, CreateDbFixture, SetUpPostgresFixture):
    @property
    def revisions_dir(self) -> Path:
        return revisions_dir()

    @property
    def base_model_cls(self) -> Any:
        return base_model_cls()

    async def test(self) -> None:
        await self.run_test()
