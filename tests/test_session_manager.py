# ruff: noqa: FBT001, FBT003, F841, TRY301

from contextlib import AbstractAsyncContextManager, asynccontextmanager
from pathlib import Path
from typing import Any, AsyncGenerator, Callable, Literal
from uuid import UUID, uuid4

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from postgres_interact import AsyncNestedSession, IsolationLevel
from postgres_interact.session_manager.errors import SessionCantBeProvidedError
from postgres_interact.tests import (
    CreateDbFixture,
    CreateSessionManagerFixture,
    MigrateDBFixture,
    SetUpPostgresFixture,
)

from .models import ItemModel
from .utils import base_model_cls, revisions_dir


class SupportError(Exception):
    pass


class TestCreateSessionManagerFixture(
    CreateSessionManagerFixture,
    MigrateDBFixture,
    CreateDbFixture,
    SetUpPostgresFixture,
):
    @property
    def revisions_dir(self) -> Path:
        return revisions_dir()

    @property
    def base_model_cls(self) -> Any:
        return base_model_cls()

    async def _do_some_operations(
        self,
        additional_resource: Callable[[], AbstractAsyncContextManager],
        action: Literal["commit", "rollback", "nothing", "raise"],
    ) -> UUID | None:
        data = "some_data"

        try:
            async with additional_resource(), self.session_manager.begin_session() as session:
                id_ = uuid4()
                session.add(ItemModel(id=id_, data=data))
                if action == "commit":
                    await session.commit()
                elif action == "rollback":
                    await session.rollback()
                elif action == "raise":
                    raise SupportError
        except SupportError:
            pass

        async with additional_resource(), self.session_manager.begin_session() as session:
            item_id = (await session.execute(select(ItemModel.id).where(ItemModel.id == id_))).scalar()

        if action in ("commit", "nothing"):
            self.assertIsNotNone(item_id)
            self.assertEqual(item_id, id_)
        else:
            self.assertIsNone(item_id)

        return item_id

    async def test_ordinal_usage(self) -> None:
        @asynccontextmanager
        async def dummy_resource() -> AsyncGenerator[None, Any]:
            yield

        async with self.session_manager.per_request():
            await self._do_some_operations(dummy_resource, "commit")
            await self._do_some_operations(dummy_resource, "nothing")
            await self._do_some_operations(dummy_resource, "rollback")
            await self._do_some_operations(dummy_resource, "raise")

        await self._do_some_operations(self.session_manager.per_request, "commit")
        await self._do_some_operations(self.session_manager.per_request, "nothing")
        await self._do_some_operations(self.session_manager.per_request, "rollback")
        await self._do_some_operations(self.session_manager.per_request, "raise")

    async def test_comparing_levels(self) -> None:
        @asynccontextmanager
        async def really_open_session(isolation_level: IsolationLevel) -> AsyncGenerator[None, Any]:
            async with (
                self.session_manager.per_request(default_isolation_level=isolation_level),
                self.session_manager.begin_session(isolation_level=isolation_level) as session,
            ):
                yield

        with self.assertRaises(SessionCantBeProvidedError):
            async with (
                really_open_session(IsolationLevel.READ_UNCOMMITTED),
                self.session_manager.begin_session(
                    isolation_level=IsolationLevel.REPEATABLE_READ,
                    new_session_if_necessary=False,
                ),
            ):
                pass

        async def compare_sessions(level: IsolationLevel, nested: bool) -> None:
            async with self.session_manager.begin_session(
                isolation_level=level,
                new_session_if_necessary=True,
            ) as session:
                if nested:
                    self.assertTrue(isinstance(session, AsyncNestedSession))
                else:
                    self.assertTrue(isinstance(session, AsyncSession) and not isinstance(session, AsyncNestedSession))

        async with really_open_session(isolation_level=IsolationLevel.READ_UNCOMMITTED) as session:
            await compare_sessions(IsolationLevel.READ_UNCOMMITTED, True)
            await compare_sessions(IsolationLevel.READ_COMMITTED, False)
            await compare_sessions(IsolationLevel.REPEATABLE_READ, False)
            await compare_sessions(IsolationLevel.SERIALIZABLE, False)

        async with really_open_session(isolation_level=IsolationLevel.READ_COMMITTED) as session:
            await compare_sessions(IsolationLevel.READ_UNCOMMITTED, True)
            await compare_sessions(IsolationLevel.READ_COMMITTED, True)
            await compare_sessions(IsolationLevel.REPEATABLE_READ, False)
            await compare_sessions(IsolationLevel.SERIALIZABLE, False)

        async with really_open_session(isolation_level=IsolationLevel.REPEATABLE_READ) as session:
            await compare_sessions(IsolationLevel.READ_UNCOMMITTED, True)
            await compare_sessions(IsolationLevel.READ_COMMITTED, True)
            await compare_sessions(IsolationLevel.REPEATABLE_READ, True)
            await compare_sessions(IsolationLevel.SERIALIZABLE, False)

        async with really_open_session(isolation_level=IsolationLevel.SERIALIZABLE) as session:
            await compare_sessions(IsolationLevel.READ_UNCOMMITTED, True)
            await compare_sessions(IsolationLevel.READ_COMMITTED, True)
            await compare_sessions(IsolationLevel.REPEATABLE_READ, True)
            await compare_sessions(IsolationLevel.SERIALIZABLE, True)

        # session manager raise level to SERIALIZABLE in this case when no session is opened before
        async with self.session_manager.per_request(default_isolation_level=IsolationLevel.READ_UNCOMMITTED):
            await compare_sessions(IsolationLevel.SERIALIZABLE, True)
