from pathlib import Path
from typing import Any

from .models import BaseModel


def revisions_dir() -> Path:
    return Path(__file__).parent / "revisions"


def base_model_cls() -> Any:
    return BaseModel
